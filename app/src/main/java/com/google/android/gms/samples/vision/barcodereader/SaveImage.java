package com.google.android.gms.samples.vision.barcodereader;

import android.app.Activity;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Pc1 on 29/01/2018.
 */

public class SaveImage {
    public static Uri fileUri;
    static final String IMAGE_DIRECTORY_NAME = "Saved Images";

    public static void saveImageToExternalStorage(Bitmap finalBitmap, Activity activity) {
        ContextWrapper wrapper;
        File myDir;
        String root;
        Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        Boolean isSDSupportedDevice = Environment.isExternalStorageRemovable();

        if (/*isSDSupportedDevice &&*/ isSDPresent) {// yes SD-card is present
            root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            myDir = new File(root, IMAGE_DIRECTORY_NAME);
        } else {
            // Sorry
            wrapper = new ContextWrapper(activity);
            myDir = wrapper.getDir(IMAGE_DIRECTORY_NAME, MODE_PRIVATE);
        }
        // Create a media file name
        // Create the storage directory if it does not exist
        if (!myDir.exists()) {
            if (!myDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        String fname = "Image-" + timeStamp + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            fileUri = Uri.fromFile(file);
            //  finalBitmap.setDensity(100);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static Uri  getUrl() {
        return fileUri;
    }
    /*  private File createImageFile(byte[] data*//*Bitmap bitmap*//*) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  *//* prefix *//*
                ".jpg",         *//* suffix *//*
                storageDir      *//* directory *//*
        );
        // Save a file: path for use with ACTION_VIEW intents
        // mCurrentPhotoPath = image.getAbsolutePath();
        try {
            FileOutputStream out = new FileOutputStream(image);
            fileUri = Uri.fromFile(image);
            //  finalBitmap.setDensity(100);
            // bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.write(data);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }*/
}
